clear all
clc

%sta?e fizyczne

E_1=1200e9; % modu Younga diament
v_1=0.2; % wsp??czynnik Poissona diament
E_2=290e9; % modu? Younga CrN
v_2=0.2; % wsp??. Poissona CrN
mu=0.45; % wsp??. tarcia
R=100e-6; % promie? wg??bnika
d=0.1e-6; % zag??bienie wst?pne-zadajemy sami na pocz?tku, to stasndard 
a=(R*d)^(0.5); %promie? kontaktu

%obliczenia obci??e?

E_zred=(E_1*E_2)/((1-v_1^2)*E_2+(1-v_2^2)*E_1);
F=(4/3)*E_zred*R^(0.5)*d^(1.5);
P_0=(3*F)/(2*3.14*R*d);

% tworzenie tabeli obici???? i wykresu

x=linspace(0,1000e-6,1000);

for i=1:1:1000
    
    P_y(i)=P_0*(1-(1/(10^12*R*d))*(10^6*(x(i)-500e-6))^2)^0.5; % funkcja jest dostosowana poprzez 10^6 i 10^12
    P_x(i)=mu*P_y(i);
    
end


plot(x,-P_y,'b',x,P_x,'r'); % rysujemy wykres dla y na niebiesko, dla x na czerwono
xlabel('x');
ylabel('Obiazenie zew');
legend('P_y normalne','P_x styczne'); % legenda
grid on % siatka








